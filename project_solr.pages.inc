<?php

//----------------------------------------
// Page callbacks
//----------------------------------------

/**
 * Summary project browsing page.
 */
function project_solr_browse_summary_page() {
  $items = array();
  $node_types = node_type_get_types();
  $project_types = project_project_node_types();
  foreach ($project_types as $type) {
    // @TODO - possibly needs more detail displayed, though this page is not used on drupal.org.
    $items[] = l($node_types[$type]->name, 'project/' . $type);
  }

  drupal_set_title(t('Project types'));
  return theme('item_list', array('items' => $items));
}

/*
 * @todo : This page should become a search page
 */
function project_solr_browse_page($type_name) {

  $project_types = project_project_node_types();

  // If the string in the url does not match any project type,
  // then return a 404
  if (array_search($type_name, $project_types) === FALSE) {
    return drupal_not_found();
  }

  // If it is found, execute the following series of actions
  $node_types = node_type_get_types();
  $project_type = $node_types[$type_name];

  // Set the page title and breadcrumb.
  drupal_set_title($project_type->name);
  $breadcrumb = menu_get_active_breadcrumb();
  drupal_set_breadcrumb($breadcrumb);


  $output = array();

  // Print the description to the page
  if (!empty($project_type->description)) {
    $output['description']['#markup'] = filter_xss($project_type->description);
  }

  $form_state = array('method' => 'get', 'always_process' => TRUE, 'no_redirect' => TRUE, 'build_info' => array('args' => array($project_type)));
  $form = drupal_build_form('project_solr_browse_projects_form', $form_state);
  unset($form['#build_id'], $form['form_build_id'], $form['form_id']);
  $output['project_solr_browse_projects_form'] = $form;

  if (isset($form_state['storage']['results'])) {
    $output['results']['#markup'] = $form_state['storage']['results'];
  }

  return $output;
}

//----------------------------------------
// Category page and related functions
//----------------------------------------

/**
 * Page callback for the listing of per-type categories.
 *
 * @param $project_type
 *   Fully-loaded taxonomy term object for the project type.
 *
 * @return
 *   Rendered page output for the project/%project_type/categories pages.
 */
function project_solr_category_page($project_type) {
  $tree = taxonomy_get_tree(_project_get_vid(), $project_type->tid);
  if (empty($tree)) {
    return drupal_not_found();
  }

  drupal_set_title(t('@project_type categories', array('@project_type' => $project_type->name)));

  $categories = array();
  foreach ($tree as $category_term) {
    $items = project_solr_fetch_category_items($project_type, $category_term);
    if (!empty($items)) {
      $categories[$category_term->tid] = array(
        'title' => check_plain($category_term->name),
        'items' => $items,
      );
    }
  }

  $search_path = 'project/' . drupal_strtolower($project_type->name) . '/categories';
  $version_form = drupal_get_form('project_solr_version_form', $search_path);

  return theme('project_solr_category_page', $project_type, $categories, $version_form);
}

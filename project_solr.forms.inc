<?php

/**
 * Build project browsing navigation form.
 */
function project_solr_browse_projects_form($form, &$form_state, $project_type) {
  // Add css
  drupal_add_css(drupal_get_path('module', 'project_solr') .'/project_solr.css');

  $form['#method'] = 'get';
  if (user_is_logged_in()) {
    $form['#token'] = FALSE;
  }

  // Create a drop-down for each related vocabulary.
  $vocabs = array();
  $fields_info = field_info_instances('node', $project_type->type);
  foreach ($fields_info as $field_name => $value) {
    $field_info = field_info_field($field_name);
    if ($field_info['type'] == 'taxonomy_term_reference') {
      $vocabs[] = $field_info['settings']['allowed_values'][0]['vocabulary'];
    }
  }

  // Add the API compatibilty option, if applicable.
  if (module_exists('project_release')) {
    $api_vocab = taxonomy_vocabulary_load(variable_get('project_release_api_vocabulary', ''));
    $vocabs[] = $api_vocab->machine_name;
  }

  $vocab_names = taxonomy_vocabulary_get_names();
  foreach ($vocabs as $vocab) {
    $vocab = taxonomy_vocabulary_machine_name_load($vocab);
    $terms = taxonomy_get_tree($vocab->vid);
    $options = array('' => t(' - Any -'));
    foreach ($terms as $term) {
      $options[$term->tid] = $term->name;
    }

    $form['term_vid_' . $vocab->vid] = array(
      '#type' => 'select',
      '#title' => check_plain($vocab->name),
      '#options' => $options,
    );
  }

  // Add the project sandbox fields.
  $form['sm_field_project_type'] = array(
    '#title' => t('Status'),
    '#type' => 'select',
    '#options' => array(
      'full' => t('Full projects'),
      '' => t('All projects'),
      'sandbox' => t('Only sandbox projects'),
    ),
  );

  $form['text'] = array(
    '#title' => t('Search @project_type', array('@project_type' => $project_type->name)),
    '#type' => 'textfield',
    '#size' => 20,
  );

  $form['solrsort'] = array(
    '#type' => 'select',
    '#title' => t('Sort by'),
    '#options' => array(
      'score desc' => t('Relevency'),
      'sort_label asc' => t('Title'),
      'ss_name asc' => t('Author'),
      'ds_created desc' => t('Created date'),
      'ds_project_latest_release desc' => t('Last release'),
      'ds_project_latest_activity desc' => t('Last build'),
    ),
    '#default_value' => 'score desc',
  );
  if (module_exists('project_usage')) {
    $form['solrsort']['#options']['iss_project_release_usage desc'] = t('Most installed');
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),
  );
  return $form;
}

function project_solr_browse_projects_form_submit($form, &$form_state) {
  // Fetch our text and url values
  $page = isset($_GET['page']) ? $_GET['page'] : 0;
  $params['start'] = $page * variable_get('apachesolr_rows', 10);

  // Set some default solr params
  $params = array(
    'q' => $form_state['values']['text'],
    'qf' => 'content',
    'fl' => 'id,entity_id,label,content,is_comment_count,bundle,ds_created,ds_changed,url,is_uid,ss_name,iss_project_release_usage,ds_project_latest_release,ds_project_latest_activity',
    'rows' => variable_get('apachesolr_rows', 10),
    'facet' => 'true',
    'facet.mincount' => 1,
    'facet.sort' => 'true',
    'facet.field' => array(
      'bundle',
      'im_project_release_api_tids',
    ),
    'facet.limit' => 200,
  );

  // This is the object that does the communication with the solr server.
  $env_id = apachesolr_default_environment();
  $solr = apachesolr_get_solr($env_id);
  $query = new SolrBaseQuery($env_id, $solr, $params, $form_state['values']['solrsort'], $_GET['q']);
  if (is_null($query)) {
    throw new Exception(t('Could not construct a Solr query.'));
  }

  // @todo: Identify whether this is truly necessary/true anymore.
  // We add addFilter() parameters here to include all the constant
  // filters for the query -- project nodes of the given top-level type that
  // have releases (if project_release is enabled).
  // We use addFilter() rather than $params['fq'] so that our filters
  // are correctly passed to anything that uses our cached query.
  $query->addFilter('bundle', arg(1));
  // We can only filter on bs_project_has_releases for just official projects,
  // since sandbox projects can never have releases.
  if (module_exists('project_release') && $form_state['values']['sm_field_project_type'] == 'full') {
    $query->addFilter('bs_field_project_has_releases', 1);
  }

  // Apply term filters
  foreach ($form_state['values'] as $key => $value) {
    if (strpos($key, 'term_vid_') === 0 && $value !== '') {
      $vid = str_replace('term_vid_', '', $key);
      $query->addFilter('im_vid_' . $vid, $value);
    }
  }

  // Apply sort value.
  $query->setAvailableSort('ss_name', array('title' => t('Author'), 'default' => 'asc'));
  $query->setAvailableSort('ds_created', array('title' => t('Created date'), 'default' => 'desc'));
  $query->setAvailableSort('ds_project_latest_release', array('title' => t('Last release'), 'default' => 'desc'));
  $query->setAvailableSort('ds_project_latest_activity', array('title' => t('Last build'), 'default' => 'desc'));
  if (module_exists('project_usage')) {
    $query->setAvailableSort('iss_project_release_usage', array('title' => t('Most installed'), 'default' => 'desc'));
  }
  list($sort_field, $sort_direction) = explode(" ", $form_state['values']['solrsort']);
  $query->setSolrsort($sort_field, $sort_direction);

  // Allow modules to alter the query prior to statically caching it.
  // This can e.g. be used to add available sorts.
  foreach (module_implements('apachesolr_query_prepare') as $module) {
    $function_name = $module . '_apachesolr_query_prepare';
    $function_name($query, $params, 'project_solr_browse_page');
  }

  // Cache the built query. Since all the built queries go through
  // this process, all the hook_invocations will happen later.
  apachesolr_current_query($env_id, $query);

  if (!$query) {
    return array();
  }

  $response = $query->search($form_state['values']['text']);

  // The response is cached so that it is accessible to the blocks and anything
  // else that needs it beyond the initial search.
  $total = $response->response->numFound;
  $searcher = $query->getSearcher();
  apachesolr_static_response_cache($searcher, $response);
  apachesolr_has_searched($env_id, TRUE);

  $output = '<div id="project-overview">';

  // Show results if any
  if ($total > 0) {
    foreach ($response->response->docs as $doc) {
      $output .= project_solr_render_search_result($doc);
    }
  }
  else {
    $output .= t('No projects found in this category.');
  }

  // Print output
  $output .= '</div>'; // id="project-overview"

  $form_state['storage']['results'] = $output;

  // @TODO - need a pager
  // theme('pager', array());
}

//----------------------------------------
// Version-selector form
//----------------------------------------

/**
 * This generates a form containing version selection and a submit button.
 *
 * Generate a form with a version selection to allow filtering page content
 * based on the API compatibility version. Also includes path to allow the
 * form to potentially submit to other urls if desired.
 *
 * @param string $path
 *   The base path to which the version form will redirect.
 * @param string $label
 *   An optional label for the form element.
 */
function project_solr_version_form(&$form_state, $path, $label = NULL) {
  $env_id = apachesolr_default_environment();
  $query = apachesolr_current_query($env_id);

  $form = array(
    '#attributes' => array('class' => 'clear-block'),
  );

  // Add version select field to our form.
  $version_alias = variable_get('project_solr_project_release_api_tids_alias', 'drupal_core');

  $form[$version_alias] = project_solr_get_api_version_field($query, $label);

  $form['path'] = array(
    '#type' => 'value',
    '#value' => $path,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),
  );
  return $form;
}

/**
 * Create a query with the right version filter and redirect to the right page.
 *
 * Create a new query, add any version filtering if it was selected in the
 * form, and redirect back to the relevant page with the appropriate filter
 * string.
 */
function project_solr_version_form_submit($form, &$form_state) {
  // We create a new query with our base path so that we don't need to remove
  // any existing drupal_core selection, and so that the implict type and
  // module tid filters don't end up in the url string.
  $query = apachesolr_drupal_query('project_solr_version_form_submit', array(), '', $form_state['values']['path']);
  $filters = array();

  $version_alias = variable_get('project_solr_project_release_api_tids_alias', 'drupal_core');

  if (!empty($form_state['values'][$version_alias])) {
    $filters = array('f' => array($version_alias . ':' . $form_state['values'][$version_alias]));
  }
  $form_state['redirect'] = array($query->getPath(), $filters);
}

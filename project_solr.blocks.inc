<?php

/**
 * Block function for the solr categories block
 * @return type
 */
function project_solr_block_project_solr_categories($query, $response) {
  // Find the parent term for this query.
  $project_vid = _project_get_vid();
  if (isset($response->responseHeader->params->fq)) {
    foreach ($response->responseHeader->params->fq as $query_snippet) {
      if (preg_match('/^im_vid_'. _project_get_vid() .':(.*)$/', $query_snippet, $matches)) {
        $tid = trim($matches[1]);
        $term = taxonomy_get_term($tid);
        if ($term->vid == $project_vid) {
          $project_type = $term;
          break;
        }
      }
    }
  }

  if (!isset($project_type)) {
    // This facet cannot process generic queries.
    return;
  }

  $facet = 'im_vid_'.  _project_get_vid();
  $terms = array();

  // Get the terms at the current depth.
  $current_tid = $project_type->tid;
  foreach ($query->getFilters() as $filter) {
    if ($filter['#name'] == 'tid') {
      $current_tid = $filter['#value'];
      break;
    }
  }
  $current_level_terms = array();
  $tree = taxonomy_get_tree(_project_get_vid(), $current_tid, -1, 1);
  foreach ($tree as $term) {
    $current_level_terms[$term->tid] = $term;
  }

  foreach ($response->facet_counts->facet_fields->$facet as $tid => $count) {
    $active = $query->hasFilter('tid', $tid);
    if (!isset($current_level_terms[$tid]) && (!$active || $tid != $current_tid)) {
      continue;
    }
    $unclick_link = '';
    $term = taxonomy_get_term($tid);
    $new_query = clone $query;

    $path = $new_query->getPath();
    $options = array();
    if ($active) {
      $new_query->removeFilter('tid', $term->tid);
      $options['query'] = $new_query->getSolrsortUrlQuery();
      $link = theme('apachesolr_unclick_link', $term->name, $path, $options);
    }
    else {
      $new_query->addFilter('tid', $term->tid);
      $options['query'] = $new_query->getSolrsortUrlQuery();
      $link = theme('apachesolr_facet_link', $term->name, $path, $options, $count, $active, $response->numFound);
    }
    $countsort = $count == 0 ? '' : 1 / $count;
    // if numdocs == 1 and !active, don't add.
    if ($response->numFound == 1 && !$active) {
      // skip
    }
    else {
      $terms[$active ? $countsort . $term->name : 1 + $countsort . $term->name] = $link;
    }
  }
  $vocab = taxonomy_vocabulary_load(_project_get_vid());

  if (!empty($terms)) {
    ksort($terms);

    // The currently selected term should be first.
    if (isset($terms[$current_tid])) {
      $current_term = $terms[$current_tid];
      unset($terms[$current_tid]);
      $terms = array_merge(array($current_tid => $current_term), $terms);
    }

    return array(
      'subject' => $vocab->name,
      'content' => theme('apachesolr_facet_list', $terms, 200),
    );
  }
}

function project_solr_block_project_solr_compatibility($query, $response) {
  if (module_exists('project_release')) {
    $facet = variable_get('project_solr_project_release_api_tids_alias', 'drupal_core');
    $terms = array();
    $active_term_counts = array();

    $active_terms = project_release_compatibility_list();

    if (isset($response->facet_counts->facet_fields->$facet)) {
      foreach ($response->facet_counts->facet_fields->$facet as $tid => $count) {
        if (!empty($active_terms[$tid])) {
          $active_term_counts[$tid] = $count;
        }
      }
    }

    foreach ($active_terms as $tid => $term_name) {
      if (!empty($active_term_counts[$tid])) {
        $active = $query->hasFilter($facet, $tid);
        $new_query = clone $query;
        $path = $new_query->getPath();
        $new_query->removeFilter($facet, $term->tid);
        $options = array();
        if ($active) {
          $options['query'] = $new_query->getSolrsortUrlQuery();
          $link = theme('apachesolr_unclick_link', $term_name, $path, $options);
        }
        else {
          $new_query->add_filter($facet, $tid);
          $options['query'] = project_solr_append_api_term($new_query->get_url_queryvalues(), $tid);
          $link = theme('project_solr_no_count_facet_link', $term_name, $path, $options, $active, $response->response->numFound);
        }
        $terms[$term_name] = $link;
      }
    }

    if (!empty($terms)) {
      return array(
        'subject' => t('Filter by compatibility'),
        'content' => theme('apachesolr_facet_list', $terms, 200),
      );
    }
  }
}